/*
 * Copyright (c) 2018, Behnam Pourghassemi, Ardalan Amiri Sani and Aparna Chandramowlishwaran,
 *                     University of California, Irvine 
 * Copyright (c) 2015, Charlie Curtsinger and Emery Berger,
 *                     University of Massachusetts Amherst
 * This file is part of the coz-plus project. See LICENSE.md file at the top-level
 * directory of this distribution and at https://gitlab.com/coz-plus/coz-plus
 */

#include <sys/time.h>
#include <execinfo.h>

#include <boost/interprocess/managed_shared_memory.hpp>

#include <dlfcn.h>
#include <linux/limits.h>
#include <pthread.h>
#include <stdlib.h>
#include <sys/stat.h>

#include <sstream>
#include <string>
#include <unordered_set>

#include "inspect.h"
#include "profiler.h"
#include "progress_point.h"
#include "real.h"
#include "util.h"

#include "ccutil/log.h"

// Include the client header file
#include "coz.h"


pthread_mutex_t* lock_pointer_ = 0;
struct timeval lock_tstart,lock_tfinish;
int lock_owner;

#ifndef CHROME_PATH
  #define CHROME_PATH /home/behnam/Desktop/webKitProject/chromium/src/out/first_build
#endif
#define LIBBLINK_CORE "CHROME_PATH/libblink_core.so"
#define LIBCONTENT "CHROME_PATH/libcontent.so"
#define LIBBASE "CHROME_PATH/libbase.so"
#define LIBCC "CHROME_PATH/libcc.so"
#define LIBCC_BASE "CHROME_PATH/libcc_base.so"
#define LIBV8 "CHROME_PATH/libv8.so"

#define DOCUMENTLOADERCPP "/home/behnam/Desktop/webKitProject/chromium/src/third_party/WebKit/Source/core/loader/DocumentLoader.cpp"

#define HTMLDOCUMENTPARSERCPP "/home/behnam/Desktop/webKitProject/chromium/src/third_party/WebKit/Source/core/html/parser/HTMLDocumentParser.cpp"
#define BACKGROUNDHTMLPARSER "/home/behnam/Desktop/webKitProject/chromium/src/third_party/WebKit/Source/core/html/parser/BackgroundHTMLParser.cpp"

#define DOCUMENTH "/home/behnam/Desktop/webKitProject/chromium/src/third_party/WebKit/Source/core/dom/Document.h"
#define DOCUMENTCPP "/home/behnam/Desktop/webKitProject/chromium/src/third_party/WebKit/Source/core/dom/Document.cpp"
#define SYLERULEIMPORTCPP "/home/behnam/Desktop/webKitProject/chromium/src/third_party/WebKit/Source/core/css/StyleRuleImport.cpp"
#define LINKSTYLECPP "/home/behnam/Desktop/webKitProject/chromium/src/third_party/WebKit/Source/core/html/LinkStyle.cpp"
#define FONTFACESETCPP "/home/behnam/Desktop/webKitProject/chromium/src/third_party/WebKit/Source/core/css/FontFaceSet.cpp"
#define CSSPARSERCPP "/home/behnam/Desktop/webKitProject/chromium/src/third_party/WebKit/Source/core/css/parser/CSSParser.cpp"

#define FRAMEPAINTERCPP "/home/behnam/Desktop/webKitProject/chromium/src/third_party/WebKit/Source/core/paint/FramePainter.cpp"
#define LOCALFRAMEVIEWCPP "/home/behnam/Desktop/webKitProject/chromium/src/third_party/WebKit/Source/core/frame/LocalFrameView.cpp"
#define GRAPHICSLAYERCPP "/home/behnam/Desktop/webKitProject/chromium/src/third_party/WebKit/Source/platform/graphics/GraphicsLayer.cpp"
#define LAYOUTVIEWCPP "/home/behnam/Desktop/webKitProject/chromium/src/third_party/WebKit/Source/core/layout/LayoutView.cpp"
#define LAYOUTBLOCKCPP "/home/behnam/Desktop/webKitProject/chromium/src/third_party/WebKit/Source/core/layout/LayoutBlock.cpp"

#define V8SCRIPTRUNNERCPP "/home/behnam/Desktop/webKitProject/chromium/src/third_party/WebKit/Source/bindings/core/v8/V8ScriptRunner.cpp"
#define V8SCHEDULEDACTIONCPP "/home/behnam/Desktop/webKitProject/chromium/src/third_party/WebKit/Source/bindings/core/v8/ScheduledAction.cpp"
#define V8SCRIPTCONTROLLERCPP "/home/behnam/Desktop/webKitProject/chromium/src/third_party/WebKit/Source/bindings/core/v8/ScriptController.cpp"
using namespace std;

/// The type of a main function
typedef int (*main_fn_t)(int, char**, char**);

/// The program's real main function
main_fn_t real_main;

static bool end_to_end = false;
bool initialized = false;
static bool init_in_progress = false;


/**
 * Called by the application to get/create a progress point
 */
extern "C" coz_counter_t* _coz_get_counter(progress_point_type t, const char* name) {
  if(t == progress_point_type::throughput) {
    throughput_point* p = profiler::get_instance().get_throughput_point(name);
    if(p) return p->get_counter_struct();
    else return nullptr;
    
  } else if(t == progress_point_type::begin) {
    latency_point* p = profiler::get_instance().get_latency_point(name);
    if(p) return p->get_begin_counter_struct();
    else return nullptr;
    
  } else if(t == progress_point_type::end) {
    latency_point* p = profiler::get_instance().get_latency_point(name);
    if(p) return p->get_end_counter_struct();
    else return nullptr;
    
  } else {
    WARNING << "Unknown progress point type " << ((int)t) << " named " << name;
    return nullptr;
  }
}

#ifdef MY_FIXED_LINE
extern "C" void turnOnMyExperiment(){
  profiler::get_instance()._my_experiment.store(3);
}

extern "C" void turnOffMyExperiment(){
  profiler::get_instance()._my_experiment.store(0);
}

#endif

/**
 * Read a link's contents and return it as a string
 */
static string readlink_str(const char* path) {
  size_t exe_size = 1024;
  ssize_t exe_used;

  while(true) {
    char exe_path[exe_size];

    exe_used = readlink(path, exe_path, exe_size - 1);
    REQUIRE(exe_used > 0) << "Unable to read link " << path;

    if(exe_used < exe_size - 1) {
      exe_path[exe_used] = '\0';
      return string(exe_path);
    }

    exe_size += 1024;
  }
}

/*
 * Initialize coz.  This will either happen as main() is called using
 * __libc_start_main, or on the first call to pthread_create() (if
 * that happen earlier, which might happen if some shared library
 * dependency create a thread in its initializer).
 */
#ifndef SHARED_MEMORY_MAP
  void init_coz(void) {
#else
  void init_coz(int type) {
#endif
  if (init_in_progress) {
    INFO << "init_coz in progress, do not recurse";
    return;
  }
  init_in_progress = true;
  INFO << "bootstrapping coz";
  initialized = false;

  // Remove Coz from LD_PRELOAD. Just clearing LD_PRELOAD for now FIXME!
  #ifndef DEBUG_PROCESS
  unsetenv("LD_PRELOAD");
  #endif
  // Read settings out of environment variables
  string output_file = getenv_safe("COZ_OUTPUT", "profile.coz");

  vector<string> binary_scope_v = split(getenv_safe("COZ_BINARY_SCOPE"), '\t');
  unordered_set<string> binary_scope(binary_scope_v.begin(), binary_scope_v.end());

  vector<string> source_scope_v = split(getenv_safe("COZ_SOURCE_SCOPE"), '\t');
  unordered_set<string> source_scope(source_scope_v.begin(), source_scope_v.end());

  vector<string> progress_points_v = split(getenv_safe("COZ_PROGRESS_POINTS"), '\t');
  unordered_set<string> progress_points(progress_points_v.begin(), progress_points_v.end());

  end_to_end = getenv("COZ_END_TO_END");
  string fixed_line_name = getenv_safe("COZ_FIXED_LINE", "");
  int fixed_speedup;
  stringstream(getenv_safe("COZ_FIXED_SPEEDUP", "-1")) >> fixed_speedup;

  // Replace 'MAIN' in the binary_scope with the real path of the main executable
  if(binary_scope.find("MAIN") != binary_scope.end()) {
    binary_scope.erase("MAIN");
    string main_name = readlink_str("/proc/self/exe");
    binary_scope.insert(main_name);
    INFO << "Including MAIN, which is " << main_name;
  }

  #ifdef MY_FIXED_LINE
    binary_scope.insert(LIBBLINK_CORE);
    binary_scope.insert(LIBV8);
    //binary_scope.insert(LIBCONTENT);
    source_scope.clear();
    if(fixed_line_name == "oth"){
      source_scope.insert(DOCUMENTLOADERCPP);
    }
    source_scope.insert(HTMLDOCUMENTPARSERCPP);
    source_scope.insert(BACKGROUNDHTMLPARSER);

    source_scope.insert(SYLERULEIMPORTCPP);
    source_scope.insert(DOCUMENTCPP);
    source_scope.insert(LINKSTYLECPP);
    source_scope.insert(FONTFACESETCPP);
    source_scope.insert(CSSPARSERCPP);

    source_scope.insert(LOCALFRAMEVIEWCPP);
    source_scope.insert(FRAMEPAINTERCPP);
    source_scope.insert(GRAPHICSLAYERCPP);

    source_scope.insert(V8SCRIPTRUNNERCPP);
    source_scope.insert(V8SCHEDULEDACTIONCPP);
    source_scope.insert(V8SCRIPTCONTROLLERCPP);

    source_scope.insert(LAYOUTVIEWCPP);
    source_scope.insert(LAYOUTBLOCKCPP);


//source_scope.clear();
//source_scope.insert("%");
  #endif
  std::cout<<"binary scope: "<<binary_scope.size();
  for(auto& i : binary_scope)	
    std::cout<<i<<endl;
	
  std::cout<<"source scope: "<<source_scope.size();
  for(auto& i : source_scope)	
    std::cout<<i<<endl;

  // Build the memory map for all in-scope binaries
  #ifndef SHARED_MEMORY_MAP
    memory_map::get_instance().build(binary_scope, source_scope);
  #else
    if(type == 0) //parent process. e.g. browser process
      memory_map::get_instance().build_and_record(binary_scope, source_scope);
    else  //child process. e.g. renderer process
      memory_map::get_instance().build_from_shared_memory();
  #endif
  std::cout<<"ranges: "<<memory_map::get_instance().ranges().size()<<endl;


  shared_ptr<line> fixed_line;
  if(fixed_line_name != "") {
    fixed_line = memory_map::get_instance().find_line(fixed_line_name);
    PREFER(fixed_line) << "Fixed line \"" << fixed_line_name << "\" was not found.";
  }

  // Create an end-to-end progress point and register it if running in
  // end-to-end mode
  if(end_to_end) {
    (void)profiler::get_instance().get_throughput_point("end-to-end");
  }

  // Start the profiler
 
  profiler::get_instance().startup(output_file,
                                   fixed_line.get(),
                                   fixed_speedup,
                                   end_to_end
				   #ifdef MY_FIXED_LINE
				     , fixed_line_name
				   #endif
				   );

  // Synchronizations can be intercepted once the profiler has been initialized
  initialized = true;
  init_in_progress = false;
}


#ifdef DEBUG_PROCESS
using namespace boost::interprocess;

struct TestStruct{
 int _a;
 double _b;
   std::map<std::string, std::shared_ptr<int>> _int;
 TestStruct():_a(5),_b(6.2){}
 void addInt(std::string s, std::shared_ptr<int> p){_int.insert(std::pair<std::string,std::shared_ptr<int>>(s,p));}
 int getSize(){return _int.size();}
 void insert(int a, double b){
  _a = a;
  _b = b;
  } 
  TestStruct(const TestStruct&)= delete;
  TestStruct& operator=(const TestStruct&) = delete;
};

void init_shared_memory(){
  try{
    boost::interprocess::shared_memory_object::remove("memory_map");
    boost::interprocess::managed_shared_memory segment(create_only, "memory_map", 65536);
    //boost::interprocess::mapped_region(shm, read_write);
    TestStruct *testStruct = segment.construct<TestStruct>("first")[2]();
    TestStruct *tmp = segment.find<TestStruct>("first").first;
    tmp[1].insert(1,3.1);
    int a = 7;
    std::shared_ptr<int> shp(&a);
    tmp[1].addInt("hello",shp);
  }catch (const boost::interprocess::interprocess_exception &ex) {
     std::cout << "managed_shared_memory ex: "  << ex.what();
  }
}


void use_shared_memory(){
  try{
    boost::interprocess::managed_shared_memory segment(open_only, "memory_map");
    TestStruct *tmp = segment.find<TestStruct>("first").first;
    WARNING<<"data for first struct: "<<tmp[0]._a<<" "<<tmp[0]._b<<" "<<tmp[0].getSize();
    WARNING<<"data for second struct: "<<tmp[1]._a<<" "<<tmp[1]._b<<" "<<tmp[1].getSize();
    tmp[0].insert(1,3.1);
    segment.destroy<TestStruct>("frist");
    
  }catch (const boost::interprocess::interprocess_exception &ex) {
     std::cout << "managed_shared_memory ex: "  << ex.what();
  }
}
#endif





/**
 * Pass the real __libc_start_main this main function, then run the real main
 * function. This allows Coz to shut down when the real main function returns.
 */
static int wrapped_main(int argc, char** argv, char** env) {
#ifdef DEBUG_PROCESS
  #ifdef SHARED_MEMORY_MAP
  //FIXME a hack to capture "browser process"
/*  bool is_browser_process = 1;
  for(int i = 0; i < argc; i++){
    if (std::strstr(argv[i], "--type=") != NULL){
      is_browser_process = 0;
      break;
    }
  }  */
  if((strcmp(argv[0], "../chromium/src/out/first_build/chrome") == 0 || (strcmp(argv[0],"/home/behnam/Desktop/webKitProject/chromium/src/out/first_build/chrome") == 0 && strcmp(argv[1],"--no-zygote") == 0))||(strcmp(argv[0], "../chromium/src/out/first_build/content_shell") == 0 || (strcmp(argv[0],"/home/behnam/Desktop/webKitProject/chromium/src/out/first_build/content_shell") == 0 && strcmp(argv[1],"--no-zygote") == 0)))
  //if(is_browser_process)
    if(!initialized){
      init_coz(0);
    }
  INFO<<argv[0]<<" "<<argv[1];
  #endif
  //FIXME a hack to capture "renderer process"
  bool extension_process = 0;
  for(int i = 0; i < argc; i++)
    if(strcmp(argv[i], "--type=renderer") == 0){
      for(int j = 0; j < argc; j++) //this is to discard renderer process that mark as extension process
        if(strcmp(argv[j], "--extension-process") == 0)
          extension_process = 1;
      if(extension_process)
        break;
      WARNING<<"RENDERER PROCESS STARTED PID:"<<getpid();
      WARNING<<"ranges: "<<memory_map::get_instance().ranges().size();
      //use_shared_memory();
#endif
  if (!initialized){
  #ifndef SHARED_MEMORY_MAP
    init_coz();
  #else
    init_coz(1);
    //printRanges("render_process_ranges_orig.txt");
    //memory_map::get_instance().remove_base_for_blink();
    //printRanges("render_process_ranges_r.txt");
  #endif
    }
#ifdef DEBUG_PROCESS
    break;
  }
#endif
   #ifdef DEBUG_SAMPLER
     profiler::get_instance().start_t = get_time();
   #endif

  // Run the real main function
  int result = real_main(argc, argv, env);

  // Increment the end-to-end progress point just before shutdown
  if(end_to_end) {
    throughput_point* end_point =
      profiler::get_instance().get_throughput_point("end-to-end");
    end_point->visit();
  }

  // Shut down the profiler
  profiler::get_instance().shutdown();

  return result;
}

/**
 * Interpose on the call to __libc_start_main to run before libc constructors.
 */

#ifdef CHROME_EVENTS

static bool resolving = false;        //< Set to true while symbol resolution is in progress
static bool in_dlopen = false;        //< Set to true while dlopen is running
static void* trace_event_handle = NULL;   //< The `dlopen` handle to libbase

static void* get_trace_event_handle() {
  if(trace_event_handle == NULL && !__atomic_exchange_n(&in_dlopen, true, __ATOMIC_ACQ_REL)) {
    trace_event_handle = dlopen("libbase.so", RTLD_NOW | RTLD_GLOBAL | RTLD_NOLOAD);
    __atomic_store_n(&in_dlopen, false, __ATOMIC_RELEASE);
  }

  return trace_event_handle;
}


extern "C" {
namespace base {

class TimeTicks {};

namespace trace_event {

struct TraceEventHandle {
  uint32_t chunk_seq;
  unsigned chunk_index : 26;
  unsigned event_index : 6;
};

TraceEventHandle traceHandle;

class ConvertableToTraceFormat {};

class TraceLog {
 public:
TraceEventHandle AddTraceEventWithThreadIdAndTimestamp(
    char phase,
    const unsigned char* category_group_enabled,
    const char* name,
    const char* scope,
    unsigned long long id,
    unsigned long long bind_id,
    int thread_id,
    const TimeTicks& timestamp,
    int num_args,
    const char** arg_names,
    const unsigned char* arg_types,
    const unsigned long long* arg_values,
    std::unique_ptr<ConvertableToTraceFormat>* convertable_values,
    unsigned int flags);
};

TraceEventHandle (* real_AddTraceEventWithThreadIdAndTimestamp)(char, const unsigned char*, const char*, const char*, unsigned long long, unsigned long long, int, const TimeTicks&, int, const char**, const unsigned char*, const unsigned long long*, std::unique_ptr<ConvertableToTraceFormat>*, unsigned int) = nullptr;

TraceEventHandle TraceLog::AddTraceEventWithThreadIdAndTimestamp(
    char phase,
    const unsigned char* category_group_enabled,
    const char* name,
    const char* scope,
    unsigned long long id,
    unsigned long long bind_id,
    int thread_id,
    const TimeTicks& timestamp,
    int num_args,
    const char** arg_names,
    const unsigned char* arg_types,
    const unsigned long long* arg_values,
    std::unique_ptr<ConvertableToTraceFormat>* convertable_values,
    unsigned int flags){
  auto handle = get_trace_event_handle();
  while(!__atomic_exchange_n(&resolving, true, __ATOMIC_ACQ_REL)) {}
  uintptr_t addr = reinterpret_cast<uintptr_t>(dlsym(handle, "base::trace_event::TraceLog::AddTraceEventWithThreadIdAndTimestamp"));
  memcpy(&real_AddTraceEventWithThreadIdAndTimestamp, &addr, sizeof(uintptr_t));
  __atomic_store_n(&resolving, false, __ATOMIC_RELEASE);
  if(real_AddTraceEventWithThreadIdAndTimestamp) {
    return real_AddTraceEventWithThreadIdAndTimestamp(phase, category_group_enabled, name, scope, id ,bind_id, thread_id, 
						timestamp, num_args, arg_names, arg_types, arg_values, convertable_values, flags);
  }
  else 
    return traceHandle;  // Silently elide synchronization during linking
}

} //namespace base
} // namespace trace_event
} // extern

extern "C" {
namespace webrtc {
namespace trace_event_internal {
static inline void AddTraceEvent(char phase,
                                const unsigned char* category_enabled,
                                const char* name,
                                unsigned long long id,
                                unsigned char flags) {
}

}
}
}

extern "C" {
namespace webrtc {
class EventTracer {
 public:

  static void AddTraceEvent(char phase,
                            const unsigned char* category_enabled,
                            const char* name,
                            unsigned long long id,
                            int num_args,
                            const char** arg_names,
                            const unsigned char* arg_types,
                            const unsigned long long* arg_values,
                            unsigned char flags);
};
void EventTracer::AddTraceEvent(char phase,
                                const unsigned char* category_enabled,
                                const char* name,
                                unsigned long long id,
                                int num_args,
                                const char** arg_names,
                                const unsigned char* arg_types,
                                const unsigned long long* arg_values,
                                unsigned char flags)  {
}
}
}

#endif  //CHROME_EVENTS

/**
 * Interpose on the call to __libc_start_main to run before libc constructors.
 */
extern "C" int __libc_start_main(main_fn_t, int, char**, void (*)(), void (*)(), void (*)(), void*) __attribute__((weak, alias("coz_libc_start_main")));

extern "C" int coz_libc_start_main(main_fn_t main_fn, int argc, char** argv,
    void (*init)(), void (*fini)(), void (*rtld_fini)(), void* stack_end) {
  // Find the real __libc_start_main
  auto real_libc_start_main = (decltype(__libc_start_main)*)dlsym(RTLD_NEXT, "__libc_start_main");
  // Save the program's real main function
  real_main = main_fn;
  // Run the real __libc_start_main, but pass in the wrapped main function
  int result = real_libc_start_main(wrapped_main, argc, argv, init, fini, rtld_fini, stack_end);

  return result;
}

/// Remove coz's required signals from a signal mask
static void remove_coz_signals(sigset_t* set) {
  if(sigismember(set, SampleSignal)) {
    sigdelset(set, SampleSignal);
  }
  if(sigismember(set, SIGSEGV)) {
    sigdelset(set, SIGSEGV);
  }
  if(sigismember(set, SIGABRT)) {
    sigdelset(set, SIGABRT);
  }
}

/// Check if a signal is required by coz
static bool is_coz_signal(int signum) {
  return signum == SampleSignal || signum == SIGSEGV || signum == SIGABRT;
}

extern "C" {

#ifdef DEBUG_PROCESS
  int fork(){
    pid_t pid = real::fork();
    return pid;
  }
#endif

  /// Pass pthread_create calls to coz so child threads can inherit the parent's delay count
  int pthread_create(pthread_t* thread,
                     const pthread_attr_t* attr,
                     thread_fn_t fn,
                     void* arg) {
    return profiler::get_instance().handle_pthread_create(thread, attr, fn, arg);
  }

  /// Catch up on delays before exiting, possibly unblocking a thread joining this one
  void __attribute__((noreturn)) pthread_exit(void* result) {
         profiler::get_instance().handle_pthread_exit(result);
  }

  /// Skip any delays added while waiting to join a thread
  int pthread_join(pthread_t t, void** retval) {
    if(initialized) profiler::get_instance().pre_block();
    int result = real::pthread_join(t, retval);
    if(initialized) profiler::get_instance().post_block(true);

    return result;
  }

  int pthread_tryjoin_np(pthread_t t, void** retval) throw() {
    
    if(initialized) profiler::get_instance().pre_block();
    int result = real::pthread_tryjoin_np(t, retval);
    if(initialized) profiler::get_instance().post_block(result == 0);
    return result;
  }

  int pthread_timedjoin_np(pthread_t t, void** ret, const struct timespec* abstime) {
    if(initialized) profiler::get_instance().pre_block();
    int result = real::pthread_timedjoin_np(t, ret, abstime);
    if(initialized) profiler::get_instance().post_block(result == 0);
    return result;
  }

  /// Skip any global delays added while blocked on a mutex
  int pthread_mutex_lock(pthread_mutex_t* mutex) throw() {
    if(initialized) profiler::get_instance().pre_block();
  #ifdef DEBUG_MUTEX
    char name[30],name2[30];
    pthread_getname_np(pthread_self(),name,30);
    struct timeval tstart,tfinish;
    int lock_, owner_, dur_;
    lock_ = mutex->__data.__lock;
    owner_ = mutex->__data.__owner;
    if(strcmp(name, "Chrome_InProcRe") == 0 || strcmp(name, "Chrome_ChildIOT") == 0)
      gettimeofday(&tstart, NULL);
  #endif  //DEBUG_MUTEX
    int result = real::pthread_mutex_lock(mutex);
  #ifdef DEBUG_MUTEX    
    if(strcmp(name, "Chrome_InProcRe") == 0 || strcmp(name, "Chrome_ChildIOT") == 0){
      gettimeofday(&tfinish, NULL);
      dur_ = (tfinish.tv_sec -tstart.tv_sec)*1000000+(tfinish.tv_usec-tstart.tv_usec);
      if(strcmp(name, "Chrome_InProcRe") == 0 && dur_ > 27000 && lock_) {
        lock_pointer_ = mutex;
        printf("   coz: lock: %d owner: %lu pointer: %p dur: %ld ctid: %lu name: %s target: %p\n", lock_, owner_, mutex, dur_, syscall(__NR_gettid), name, lock_pointer_);
      }
    }

    if (lock_pointer_ == mutex){
      lock_owner = mutex->__data.__owner;
      gettimeofday(&lock_tstart, NULL);
      printf("mutex %p aquaired by %d in dur: %d new count: %d. it is previousely hold by %d\n", mutex, lock_owner, dur_, mutex->__data.__count, owner_); 
    }
  #endif  //DEBUG_MUTEX

    if(initialized) profiler::get_instance().post_block(true);

    return result;
  }

  /// Catch up on delays before unblocking any threads waiting on a mutex
  int pthread_mutex_unlock(pthread_mutex_t* mutex) throw() {//*profiler::get_instance().get_output_file() << "pthread_mutex_unlock " << initialized << endl;

  #ifdef DEBUG_MUTEX 
    char name[30];
    struct timeval tstart,tfinish;
    pthread_getname_np(pthread_self(),name,30);
    if( mutex == lock_pointer_)
      gettimeofday(&tstart, NULL);
  #endif  //DEBUG_MUTEX    
    auto res = real::pthread_mutex_unlock(mutex);
    if(initialized) profiler::get_instance().catch_up();   
  #ifdef DEBUG_MUTEX
    if (lock_pointer_ == mutex){
      gettimeofday(&tfinish, NULL);
      gettimeofday(&lock_tfinish, NULL);
      printf("mutex %p is released in %lu by ctid: %d new count: %d. previous owner: %d for: %d new owner:%d\n",  mutex, (tfinish.tv_sec -tstart.tv_sec)*1000000+(tfinish.tv_usec-tstart.tv_usec), syscall(__NR_gettid), mutex->__data.__count, lock_owner, (lock_tfinish.tv_sec -lock_tstart.tv_sec)*1000000+(lock_tfinish.tv_usec-lock_tstart.tv_usec),  mutex->__data.__owner);  
    }
  #endif  //DEBUG_MUTEX
    return res;
  }

  /// Skip any delays added while waiting on a condition variable
  int pthread_cond_wait(pthread_cond_t* cond, pthread_mutex_t* mutex) {
    if(initialized) profiler::get_instance().pre_block();
    int result = real::pthread_cond_wait(cond, mutex);
    if(initialized) profiler::get_instance().post_block(true);

    return result;
  }

  /**
   * Wait on a condvar for a fixed timeout. If the wait does *not* time out, skip any global
   * delays added during the waiting period.
   */
  int pthread_cond_timedwait(pthread_cond_t* cond,
                             pthread_mutex_t* mutex,
                             const struct timespec* time) {//*profiler::get_instance().get_output_file() << "pthread_cond_timewait " << initialized << endl;
    if(initialized) profiler::get_instance().pre_block();
    int result = real::pthread_cond_timedwait(cond, mutex, time);

    // Skip delays only if the wait didn't time out
    if(initialized) profiler::get_instance().post_block(result == 0);

    return result;
  }

  /// Catchup on delays before waking a thread waiting on a condition variable
  int pthread_cond_signal(pthread_cond_t* cond) throw() {
    if(initialized) profiler::get_instance().catch_up();
    return real::pthread_cond_signal(cond);
  }

  /// Catch up on delays before waking any threads waiting on a condition variable
  int pthread_cond_broadcast(pthread_cond_t* cond) throw() {
    if(initialized) profiler::get_instance().catch_up();
    return real::pthread_cond_broadcast(cond);
  }

  /// Catch up before, and skip ahead after waking from a barrier
  int pthread_barrier_wait(pthread_barrier_t* barrier) throw() {
    if(initialized) profiler::get_instance().catch_up();
    if(initialized) profiler::get_instance().pre_block();

    int result = real::pthread_barrier_wait(barrier);

    if(initialized) profiler::get_instance().post_block(true);

    return result;
  }

  int pthread_rwlock_rdlock(pthread_rwlock_t* rwlock) throw() {
    if(initialized) profiler::get_instance().pre_block();
    int result = real::pthread_rwlock_rdlock(rwlock);
    if(initialized) profiler::get_instance().post_block(true);
    return result;
  }

  int pthread_rwlock_timedrdlock(pthread_rwlock_t* rwlock, const struct timespec* abstime) throw() {
    if(initialized) profiler::get_instance().pre_block();
    int result = real::pthread_rwlock_timedrdlock(rwlock, abstime);
    if(initialized) profiler::get_instance().post_block(result == 0);
    return result;
  }

  int pthread_rwlock_wrlock(pthread_rwlock_t* rwlock) throw() {
    if(initialized) profiler::get_instance().pre_block();
    int result = real::pthread_rwlock_wrlock(rwlock);
    if(initialized) profiler::get_instance().post_block(true);
    return result;
  }

  int pthread_rwlock_timedwrlock(pthread_rwlock_t* rwlock, const struct timespec* abstime) throw() {
    if(initialized) profiler::get_instance().pre_block();
    int result = real::pthread_rwlock_timedwrlock(rwlock, abstime);
    if(initialized) profiler::get_instance().post_block(result == 0);
    return result;
  }

  int pthread_rwlock_unlock(pthread_rwlock_t* rwlock) throw() {//*profiler::get_instance().get_output_file() << "pthread_rwlock_unlock " << initialized << endl;
    if(initialized) profiler::get_instance().catch_up();
    return real::pthread_rwlock_unlock(rwlock);
  }

  /// Run shutdown before exiting
  void __attribute__((noreturn)) exit(int status) throw() {
    profiler::get_instance().shutdown();
    real::exit(status);
  }

  /// Run shutdown before exiting
  void __attribute__((noreturn)) _exit(int status) {
    profiler::get_instance().shutdown();
  	real::_exit(status);
  }

  /// Run shutdown before exiting
  void __attribute__((noreturn)) _Exit(int status) throw() {
    profiler::get_instance().shutdown();
    real::_Exit(status);
  }

  /// Don't allow programs to set signal handlers for coz's required signals
  sighandler_t signal(int signum, sighandler_t handler) throw() {
    if(is_coz_signal(signum)) {
      return NULL;
    } else {
      return real::signal(signum, handler);
    }
  }

  /// Don't allow programs to set handlers or mask signals required for coz
  int sigaction(int signum, const struct sigaction* act, struct sigaction* oldact) throw() {
    if(is_coz_signal(signum)) {
      return 0;
    } else if(act != NULL) {
      struct sigaction my_act = *act;
      remove_coz_signals(&my_act.sa_mask);
      return real::sigaction(signum, &my_act, oldact);
    } else {
      return real::sigaction(signum, act, oldact);
    }
  }

  /// Ensure coz's signals remain unmasked
  int sigprocmask(int how, const sigset_t* set, sigset_t* oldset) throw() {*profiler::get_instance().get_output_file() << "sigprocmask " << initialized << endl;
    if(how == SIG_BLOCK || how == SIG_SETMASK) {
      if(set != NULL) {
        sigset_t myset = *set;
        remove_coz_signals(&myset);
        return real::sigprocmask(how, &myset, oldset);
      }
    }

    return real::sigprocmask(how, set, oldset);
  }

  /// Ensure coz's signals remain unmasked
  int pthread_sigmask(int how, const sigset_t* set, sigset_t* oldset) throw() {*profiler::get_instance().get_output_file() << "pthread_sigmask " << initialized << endl;
    if(how == SIG_BLOCK || how == SIG_SETMASK) {
      if(set != NULL) {
        sigset_t myset = *set;
        remove_coz_signals(&myset);

        return real::pthread_sigmask(how, &myset, oldset);
      }
    }

    return real::pthread_sigmask(how, set, oldset);
  }

  /// Catch up on delays before sending a signal to the current process
  int kill(pid_t pid, int sig) throw() {
      profiler::get_instance().catch_up();
    return real::kill(pid, sig);
  }

  /// Catch up on delays before sending a signal to another thread
  int pthread_kill(pthread_t thread, int sig) throw() {
    // TODO: Don't allow threads to send coz's signals
    if(initialized) profiler::get_instance().catch_up();
    return real::pthread_kill(thread, sig);
  }

  int pthread_sigqueue(pthread_t thread, int sig, const union sigval val) throw() {*profiler::get_instance().get_output_file() << "pthread_sigqueue " << initialized << endl;
    if(initialized) profiler::get_instance().catch_up();
    return real::pthread_sigqueue(thread, sig, val);
  }

  /**
   * Ensure a thread cannot wait for coz's signals.
   * If the waking signal is delivered from the same process, skip any global delays added
   * while blocked.
   */
  int sigwait(const sigset_t* set, int* sig) {
    sigset_t myset = *set;
    remove_coz_signals(&myset);
    siginfo_t info;

    if(initialized) profiler::get_instance().pre_block();

    int result = real::sigwaitinfo(&myset, &info);

    // Woken up by another thread if the call did not fail, and the waking process is this one
    if(initialized) profiler::get_instance().post_block(result != -1 && info.si_pid == getpid());

    if(result == -1) {
      // If there was an error, return the error code
      return errno;
    } else {
      // No need to check if sig is null because it's declared as non-null
      *sig = result;
      return 0;
    }
  }

  /**
   * Ensure a thread cannot wait for coz's signals.
   * If the waking signal is delivered from the same process, skip any added global delays.
   */
  int sigwaitinfo(const sigset_t* set, siginfo_t* info) {
    sigset_t myset = *set;
    siginfo_t myinfo;
    remove_coz_signals(&myset);

    if(initialized) profiler::get_instance().pre_block();

    int result = real::sigwaitinfo(&myset, &myinfo);

    // Woken up by another thread if the call did not fail, and the waking process is this one
    if(initialized) profiler::get_instance().post_block(result > 0 && myinfo.si_pid == getpid());

    if(result > 0 && info)
      *info = myinfo;

    return result;
  }

  /**
   * Ensure a thread cannot wait for coz's signals.
   * If the waking signal is delivered from the same process, skip any global delays.
   */
  int sigtimedwait(const sigset_t* set, siginfo_t* info, const struct timespec* timeout) {
    sigset_t myset = *set;
    siginfo_t myinfo;
    remove_coz_signals(&myset);

    if(initialized) profiler::get_instance().pre_block();

    int result = real::sigtimedwait(&myset, &myinfo, timeout);

    // Woken up by another thread if the call did not fail, and the waking process is this one
    if(initialized) profiler::get_instance().post_block(result > 0 && myinfo.si_pid == getpid());

    if(result > 0 && info)
      *info = myinfo;

    return result;
  }

  /**
   * Set the process signal mask, suspend, then wake and restore the signal mask.
   * If the waking signal is delivered from within the process, skip any added global delays
   */
  int sigsuspend(const sigset_t* set) {
    sigset_t oldset;
    int sig;
    real::sigprocmask(SIG_SETMASK, set, &oldset);
    int rc = sigwait(set, &sig);
    real::sigprocmask(SIG_SETMASK, &oldset, nullptr);
    return rc;
  }

}
