/*
 * Copyright (c) 2018, Behnam Pourghassemi, Ardalan Amiri Sani and Aparna Chandramowlishwaran,
 *                     University of California, Irvine 
 * Copyright (c) 2015, Charlie Curtsinger and Emery Berger,
 *                     University of Massachusetts Amherst
 * This file is part of the coz-plus project. See LICENSE.md file at the top-level
 * directory of this distribution and at https://gitlab.com/coz-plus/coz-plus
 */

#if !defined(CAUSAL_RUNTIME_PROFILER_H)
#define CAUSAL_RUNTIME_PROFILER_H

#define MY_FIXED_LINE
//#define DEBUG_SAMPLER
#define OVERLAPPING
//#define DEBUG_MUTEX
#define DEBUG_PROCESS   //this actually should be MULTI_PROCESS. SHARED_MEMORY and Multi-process must use this micro
#define NO_SAMPLE_SCALING

#include <atomic>
#include <cstdint>
#include <fstream>
#include <string>
#include <unordered_map>
#include <vector>

#include "coz.h"

#include "inspect.h"
#include "progress_point.h"
#include "thread_state.h"
#include "util.h"

#include "ccutil/spinlock.h"
#include "ccutil/static_map.h"

#define PERIOD 2000000
#define BATCH_SIZE 10

#ifdef MY_FIXED_LINE

//extern int COZ_EXPERIMENT_FLAG;

//HTML
  #define HTML_SPOT_1 "core/html/parser/HTMLDocumentParser.cpp:287"	//pumpPendingSpeculation -> calls ProcessTokenizedChunkFromBackgroundParser
  #define HTML_SPOT_2 "core/html/parser/HTMLDocumentParser.cpp:1129"    //pumpPendingSpeculation -> calls ProcessTokenizedChunkFromBackgroundParser
  //#define HTML_SPOT_3 "core/html/parser/HTMLDocumentParser.cpp:271"
  #define HTML_SPOT_4 "core/html/parser/BackgroundHTMLParser.cpp:152"	//pumpTokenizer in AppendDecodedBytes does parsing html in background
  #define HTML_SPOT_5 "core/html/parser/BackgroundHTMLParser.cpp:193"	//pumpTokenizer in ResumeFrom ..
  #define HTML_SPOT_6 "core/html/parser/BackgroundHTMLParser.cpp:201"	//pumpTokenizer in StartedChunkWithCheckpoint
  #define HTML_SPOT_7 "core/html/parser/BackgroundHTMLParser.cpp:206"	//pumpTokenizer in finish
//CSS
/*  #define CSS_SPOT_1 "core/dom/Document.h:1725"
  #define CSS_SPOT_2 "core/dom/Document.cpp:2097"
  #define CSS_SPOT_3 "core/dom/Document.cpp:2098"
  #define CSS_SPOT_4 "core/dom/Document.cpp:2099"
  #define CSS_SPOT_5 "core/dom/Document.cpp:2100"
  #define CSS_SPOT_6 "core/dom/Document.cpp:2101"
  #define CSS_SPOT_7 "core/dom/Document.cpp:2113"
  #define CSS_SPOT_8 "core/css/StyleRuleImport.cpp:88"
  #define CSS_SPOT_9 "core/css/StyleRuleImport.cpp:89"
  #define CSS_SPOT_10 "core/html/LinkStyle.cpp:133"
  #define CSS_SPOT_11 "core/html/LinkStyle.cpp:134"
*/
  #define CSS_SPOT_1 "core/css/parser/CSSParser.cpp:70"    //parseStyleSheet
  #define CSS_SPOT_2 "core/css/parser/CSSParser.cpp:71"	   //parseStyleSheet
  #define CSS_SPOT_3 "core/dom/Document.cpp:2113"	//updateStyle in UpdateStyleAndLayoutTree
  #define CSS_SPOT_4 "core/dom/Document.cpp:2100"	//updateActiveStyle in UpdateStyleAndLayoutTree
  #define CSS_SPOT_5 "core/css/FontFaceSet.cpp:326"	//updateActiveStyle in CSSconnectedFontFaceList   this file is changed to FontFaceSetDocument.cpp in newer versions	
  #define CSS_SPOT_6 "core/css/FontFaceSet.cpp:483"	//updateActiveStyle in resolveFontStyle
//PAINT
  #define PAINT_SPOT_1 "core/frame/LocalFrameView.cpp:3152"	//UpdateIfNeededRecursive in UpdateLifecyclePhasesInternal
  #define PAINT_SPOT_2 "core/frame/LocalFrameView.cpp:3155"	//UpdateDescendantDependentFlags in UpdateLifecyclePhasesInternal
  #define PAINT_SPOT_3 "core/frame/LocalFrameView.cpp:3156"	//CommitPendingSelection in UpdateLifecyclePhasesInternal
  #define PAINT_SPOT_4 "core/frame/LocalFrameView.cpp:3161"	//ScrollContentsIfNeededRecursive in UpdateLifecyclePhasesInternal
  #define PAINT_SPOT_5 "core/frame/LocalFrameView.cpp:3171"	//UpdateAfterCompositingChangeIfNeeded in UpdateLifecyclePhasesInternal
  #define PAINT_SPOT_6 "core/frame/LocalFrameView.cpp:3182"	//UpdateCompositedSelectionIfNeeded in UpdateLifecyclePhasesInternal
  #define PAINT_SPOT_7 "core/frame/LocalFrameView.cpp:3186"	//PrePaint in UpdateLifecyclePhasesInternal
  #define PAINT_SPOT_8 "core/frame/LocalFrameView.cpp:3193"	//paintTree in UpdateLifecyclePhasesInternal							
/*
  #define PAINT_SPOT_1 "core/paint/FramePainter.cpp:86"
  #define PAINT_SPOT_2 "core/paint/FramePainter.cpp:92"
  #define PAINT_SPOT_3 "core/frame/LocalFrameView.cpp:4711"
  #define PAINT_SPOT_4 "Source/platform/graphics/GraphicsLayer.cpp:325"
*/
//JS
/*  #define JS_SPOT_1 "Source/bindings/core/v8/V8ScriptRunner.cpp:418"
  #define JS_SPOT_2 "Source/bindings/core/v8/V8ScriptRunner.cpp:419"
  #define JS_SPOT_3 "Source/bindings/core/v8/V8ScriptRunner.cpp:420"
  #define JS_SPOT_4 "Source/bindings/core/v8/V8ScriptRunner.cpp:421"
  #define JS_SPOT_5 "Source/bindings/core/v8/V8ScriptRunner.cpp:422"
  #define JS_SPOT_6 "Source/bindings/core/v8/V8ScriptRunner.cpp:423"
  #define JS_SPOT_7 "Source/bindings/core/v8/V8ScriptRunner.cpp:440"
  #define JS_SPOT_8 "Source/bindings/core/v8/V8ScriptRunner.cpp:441"
  #define JS_SPOT_9 "Source/bindings/core/v8/V8ScriptRunner.cpp:442"
  #define JS_SPOT_10 "Source/bindings/core/v8/V8ScriptRunner.cpp:443"
  #define JS_SPOT_11 "Source/bindings/core/v8/V8ScriptRunner.cpp:590"
  #define JS_SPOT_12 "Source/bindings/core/v8/V8ScriptRunner.cpp:591"
  #define JS_SPOT_13 "Source/bindings/core/v8/V8ScriptRunner.cpp:592"
  #define JS_SPOT_14 "Source/bindings/core/v8/V8ScriptRunner.cpp:593"
*/
  #define JS_SPOT_1 "Source/bindings/core/v8/ScheduledAction.cpp:161"	//ExecuteScriptAndReturnValue in Execute
 // #define JS_SPOT_2 "Source/bindings/core/v8/ScriptController.cpp:360"	//ExecuteScriptAndReturnValue in EvaluateScriptInMainWorld
  #define JS_SPOT_2 "Source/bindings/core/v8/ScriptController.cpp:313"	//ExecuteScriptAndReturnValue in EvaluateScriptInMainWorld
  #define JS_SPOT_3 "Source/bindings/core/v8/ScriptController.cpp:314"	//ExecuteScriptAndReturnValue in EvaluateScriptInMainWorld
  #define JS_SPOT_4 "Source/bindings/core/v8/ScriptController.cpp:341"	//ExecuteScriptAndReturnValue in ExecuteScriptInIsolatedWorld
  #define JS_SPOT_5 "Source/bindings/core/v8/V8ScriptRunner.cpp:692"	//Call in callfunction
  #define JS_SPOT_6 "Source/bindings/core/v8/V8ScriptRunner.cpp:712"	//Call in callfunction
//  #define JS_SPOT_7 "Source/bindings/core/v8/V8ScriptRunner.cpp:694"	//Call in callfunction
  #define JS_SPOT_7 "Source/bindings/core/v8/V8ScriptRunner.cpp:691"	//Call in callfunction
  #define JS_SPOT_8 "Source/bindings/core/v8/V8ScriptRunner.cpp:711"	//Call in callfunction

//LAYOUT
#define LAYOUT_SPOT_1 "core/frame/LocalFrameView.cpp:1265"	//PerformLayout in updateLayout
#define LAYOUT_SPOT_2 "core/frame/LocalFrameView.cpp:1082"	//updateLayout in PerformLayout
#define LAYOUT_SPOT_3 "core/layout/LayoutView.cpp:224"	//updateLayout in PerformLayout
#define LAYOUT_SPOT_4 "core/layout/LayoutBlock.cpp:427"	//updateLayout in PerformLayout
/*  #define LAYOUT_SPOT_1 "core/dom/Document.cpp:2324"	//updateLayout in UpdateStyleAndLayout
  #define LAYOUT_SPOT_2 "core/dom/Document.cpp:3177"	//updateLayout in ImplicitClose
  #define LAYOUT_SPOT_3 "core/frame/LocalFrameView.cpp:762"	//updateLayout in AdjustViewSizeAndLayout
  #define LAYOUT_SPOT_4 "core/frame/LocalFrameView.cpp:929"	//updateLayout in ForceLayoutParentViewIfNeeded
  #define LAYOUT_SPOT_5 "core/frame/LocalFrameView.cpp:987"	//updateLayout in LayoutFromRootObject
  #define LAYOUT_SPOT_6 "core/frame/LocalFrameView.cpp:1082"	//updateLayout in PerformLayout
  #define LAYOUT_SPOT_7 "core/frame/LocalFrameView.cpp:1119"	//updateLayout in ScheduleOrPerformPostLayoutTasks
  #define LAYOUT_SPOT_8 "core/frame/LocalFrameView.cpp:1482"	//updateLayout in UpdateGeometry
  #define LAYOUT_SPOT_9 "core/frame/LocalFrameView.cpp:1942"	//updateLayout in SetFragmentAnchor
  #define LAYOUT_SPOT_10 "core/frame/LocalFrameView.cpp:3409"	//updateLayout in UpdateStyleAndLayoutIfNeededRecursiveInternal
  #define LAYOUT_SPOT_11 "core/frame/LocalFrameView.cpp:3507"	//updateLayout in ForceLayoutForPagination
  #define LAYOUT_SPOT_12 "core/frame/LocalFrameView.cpp:3541"	//updateLayout in ForceLayoutForPagination
  #define LAYOUT_SPOT_13 "core/frame/LocalFrameView.cpp:1286"	//updateLayout in updatelayout
*/
#endif

/// Type of a thread entry function
typedef void* (*thread_fn_t)(void*);

/// The type of a main function
typedef int (*main_fn_t)(int, char**, char**);

enum {
  SampleSignal = SIGPROF, //< Signal to generate when samples are ready
  SamplePeriod = PERIOD, //< Time between samples (1ms)
  SampleBatchSize = BATCH_SIZE,   //< Samples to batch together for one processing run
  SpeedupDivisions = 20,  //< How many different speedups to try (20 = 5% increments)
  ZeroSpeedupWeight = 7,  //< Weight of speedup=0 versus other speedup values (7 = ~25% of experiments run with zero speedup)
  ExperimentMinTime = SamplePeriod * SampleBatchSize * 50,  //< Minimum experiment length
  ExperimentCoolOffTime = SamplePeriod * SampleBatchSize,   //< Time to wait after an experiment
  ExperimentTargetDelta = 5 //< Target minimum number of visits to a progress point during an experiment
};

/**
 * Argument type passed to wrapped threads
 */
struct thread_start_arg {
  thread_fn_t _fn;
  void* _arg;
  size_t _parent_delay_time;

  thread_start_arg(thread_fn_t fn, void* arg, size_t t) :
      _fn(fn), _arg(arg), _parent_delay_time(t) {}
};

void init_coz(void);
class profiler {
public:
  /// Start the profiler
  void startup(const std::string& outfile,
               line* fixed_line,
               int fixed_speedup,
               bool end_to_end
	       #ifdef MY_FIXED_LINE
		 , std::string fixed_line_name
	       #endif
	       );

  /// Shut down the profiler
  void shutdown();

  /// Get or create a progress point to measure throughput
  throughput_point* get_throughput_point(const std::string& name) {
    // Lock the map of throughput points
    _throughput_points_lock.lock();
  
    // Search for a matching point
    auto search = _throughput_points.find(name);
  
    // If there is no match, add a new throughput point
    if(search == _throughput_points.end()) {
      search = _throughput_points.emplace_hint(search, name, new throughput_point(name));
    }
  
    // Get the matching or inserted value
    throughput_point* result = search->second;
  
    // Unlock the map and return the result
    _throughput_points_lock.unlock();
    return result;
  }
  
  /// Get or create a progress point to measure latency
  latency_point* get_latency_point(const std::string& name) {
    // Lock the map of latency points
    _latency_points_lock.lock();
  
    // Search for a matching point
    auto search = _latency_points.find(name);
  
    // If there is no match, add a new latency point
    if(search == _latency_points.end()) {
      search = _latency_points.emplace_hint(search, name, new latency_point(name));
    }
  
    // Get the matching or inserted value
    latency_point* result = search->second;
  
    // Unlock the map and return the result
    _latency_points_lock.unlock();
    return result;
  }

  /// Pass local delay counts and excess delay time to the child thread
  int handle_pthread_create(pthread_t* thread,
                            const pthread_attr_t* attr,
                            thread_fn_t fn,
                            void* arg) {
    thread_start_arg* new_arg;

    thread_state* state = get_thread_state();
    if (NULL == state) { 
      #ifdef DEBUG_PROCESS
        return real::pthread_create(thread, attr, fn, arg);
      #endif
      init_coz();
      state = get_thread_state();
    }
    REQUIRE(state) << "Thread state not found";

    // Allocate a struct to pass as an argument to the new thread
    new_arg = new thread_start_arg(fn, arg, state->local_delay);

    // Create a wrapped thread and pass in the wrapped argument
    return real::pthread_create(thread, attr, profiler::start_thread, new_arg);
  }

  /// Force threads to catch up on delays, and stop sampling before the thread exits
  void handle_pthread_exit(void* result) __attribute__((noreturn)) {
    end_sampling();
    real::pthread_exit(result);
  }

  /// Ensure a thread has executed all the required delays before possibly unblocking another thread
  void catch_up() {
    thread_state* state = get_thread_state();

    if(!state)
      return;

    // Handle all samples and add delays as required
    if(_experiment_active) {
      state->set_in_use(true);
      add_delays(state);
      state->set_in_use(false);
    }
  }

  /// Call before (possibly) blocking
  void pre_block() {
    thread_state* state = get_thread_state();
    if(!state)
      return;

    state->pre_block_time = _global_delay.load();
  }

  /// Call after unblocking. If by_thread is true, delays will be skipped
  void post_block(bool skip_delays) {
    thread_state* state = get_thread_state();
    if(!state)
      return;

//        pthread_t self;
//	self = pthread_self();
//	char   threadName[100];
//	pthread_getname_np(self,threadName,100);
//if(!skip_delays)
// printf("post block: %d %s %d\n",gettid(),threadName,_global_delay.load() - state->pre_block_time );
 
    state->set_in_use(true);

    if(skip_delays) {  
      // Skip all delays that were inserted during the blocked period
      state->local_delay += _global_delay.load() - state->pre_block_time;
    }

    state->set_in_use(false);
  }

  /// Only allow one instance of the profiler, and never run the destructor
  static profiler& get_instance() {
    static char buf[sizeof(profiler)];
    static profiler* p = new(buf) profiler();
    return *p;
  }

#ifdef MY_FIXED_LINE
  std::atomic<int> _my_experiment;
  enum Component{HTML, PAINT, CSS, JS, LAYOUT}fixed_lines_component;
  std::map<std::string,Component> componentHash;
  std::string fixed_lines_component_str;
  void set_fixed_lines();
  void turnOnExperiment(){_experiment_active.store(true);}
  void turnOffExperiment(){_experiment_active.store(false);}
  std::ofstream* get_output_file(){return &f;}
#endif
private:
  profiler()  {
    _experiment_active.store(false);
    _global_delay.store(0);
    _delay_size.store(0);
    _selected_line.store(nullptr);
    _next_line.store(nullptr);
    _running.store(true);

    #ifdef MY_FIXED_LINE
      //COZ_EXPERIMENT_FLAG = 0;
      _my_experiment.store(-1);
      componentHash["html"] = HTML;
      componentHash["css"] = CSS;
      componentHash["js"] = JS;
      componentHash["paint"] = PAINT;
      componentHash["layout"] = LAYOUT;
      f.open("sample.txt");
    #endif

  }

  // Disallow copy and assignment
  profiler(const profiler&) = delete;
  void operator=(const profiler&) = delete;

  void profiler_thread(spinlock& l);          //< Body of the main profiler thread
  void begin_sampling(thread_state* state);   //< Start sampling in the current thread
  void end_sampling();                        //< Stop sampling in the current thread
  void add_delays(thread_state* state);       //< Add any required delays
  void process_samples(thread_state* state);  //< Process all available samples and insert delays
  line* find_line(perf_event::record&);       //< Map a sample to its source line

  #ifdef MY_FIXED_LINE
      int in_line_scope(line* l){
	int ret = 0;
	for(std::vector<std::pair<Component,line*>>::iterator itr = fixed_lines.begin(); itr != fixed_lines.end();  itr++)
	  if((*itr).second==l)
            if((*itr).first == fixed_lines_component)
	      ret = 1;
            #ifdef OVERLAPPING
            //0:no matches  -1:match line but not cat  1:match line and match cat
            else
	      ret = -1;
            #endif
        return ret;
      } 
    std::pair<line*,bool> my_find_line(perf_event::record&);
  #endif

  void log_samples(std::ofstream&, size_t);   //< Log runtime and sample counts for all identified regions

  thread_state* add_thread(); //< Add a thread state entry for this thread
  thread_state* get_thread_state(); //< Get a reference to the thread state object for this thread
  void remove_thread(); //< Remove the thread state structure for the current thread

  static void* start_profiler_thread(void*);          //< Entry point for the profiler thread
  static void* start_thread(void* arg);               //< Entry point for wrapped threads
  static void samples_ready(int, siginfo_t*, void*);  //< Signal handler for sample processing
  static void on_error(int, siginfo_t*, void*);       //< Handle errors

  /// A map from name to throughput monitoring progress points
  std::unordered_map<std::string, throughput_point*> _throughput_points;
  spinlock _throughput_points_lock; //< Spinlock that protects the throughput points map
  
  /// A map from name to latency monitoring progress points
  std::unordered_map<std::string, latency_point*> _latency_points;
  spinlock _latency_points_lock;  //< Spinlock that protects the latency points map

  static_map<pid_t, thread_state> _thread_states;   //< Map from thread IDs to thread-local state

  std::atomic<bool> _experiment_active; //< Is an experiment running?
  std::atomic<size_t> _global_delay;    //< The global delay time required
  std::atomic<size_t> _delay_size;      //< The current delay size
  std::atomic<line*> _selected_line;    //< The line to speed up
  std::atomic<line*> _next_line;        //< The next line to speed up

  pthread_t _profiler_thread;     //< Handle for the profiler thread
  std::atomic<bool> _running;     //< Clear to signal the profiler thread to quit
  std::string _output_filename;   //< File for profiler output
  line* _fixed_line;              //< The only line that should be sped up, if set
#ifdef MY_FIXED_LINE
  std::vector<std::pair<Component,line*>> fixed_lines;
  std::ofstream f;
  #ifdef DEBUG_SAMPLER
  public:
  size_t start_t;
  private:
  #endif
#endif

  int _fixed_delay_size = -1;     //< The only delay size that should be used, if set

  /// Should coz run in end-to-end mode?
  bool _enable_end_to_end;

  /// Atomic flag to guarantee shutdown procedures run exactly one time
  std::atomic_flag _shutdown_run = ATOMIC_FLAG_INIT;
};

#endif
