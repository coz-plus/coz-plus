# COZ+

COZ+ is a scalable and efficient causal profiler for web browsers. It can locate performance bottleneck of modern the web browsers and determine the impact of optimization in computation activities invloved in page loading process. COZ+ is built on top of COZ profiler with multiple optimizations and modifcations targeting large software systems and web browsers. Currently, COZ+ is customized for Chromium browser but it is adoptable to other webkit-based browsers.

## Installation

### Requirements

Linux version 2.6.32 or newer (must support the perf event system calls)

Python

Clang 3.1 or newer

c++ boost library

### Building

First, you have to build Chromium browser. You can find the instruction for building Chromium browser from source code for Linux platforms [here](https://chromium.googlesource.com/chromium/src/+/master/docs/linux_build_instructions.md).

**Make sure:**

Include debugging symbols in the executable (`is_debug=true` and `symbol_level=2`)

Use clang compiler (`is_clang=true`)

Don't use debug fission (`use_debug_fission=false`) 

**Note:**

COZ+ is tested on Chromium version *62.0.3167* and might not be compatible with other versions. Also, it is recommended to first build the [content_shell](https://www.chromium.org/developers/content-module) target and test COZ+ on that and then proceed to building Chrome.

#### build COZ+

follow the instructions to build COZ+ from source code

`git clone git@gitlab.com:coz-plus/coz-plus.git`

`cd coz-plus`

`export BOOST_ROOT=path/to/the/boost/library`

`export CHROME_ROOT=path/to/the/chrome/source/code`

`make`

## RUN COZ+

`./coz+ run -o report.log --stage js --speedup 20 --browser-path path-to-the-chrome-binary -w http://www.google.com`

It will generate information related to the requested speedup (20%) in the report.log file.

Use `./coz+ run --help` for more information.

### Acknowledge
You could find more details about our tool and what-if analysis of Chromium browser in our Sigmetrics'19 paper [here](https://dl.acm.org/citation.cfm?id=3326142). You could also read more about causal profiling and original COZ profiler [here](https://dl.acm.org/citation.cfm?id=2815409). 

